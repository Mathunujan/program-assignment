package ass.pro.bank.main;

import java.util.Scanner;

import ass.pro.bank.receipt.BankSys;
import ass.pro.bank.transaction.BankDeposit;
import ass.pro.bank.transaction.BankWithdrawal;

public class MyBank {
	public static void main(String[] args) {
		BankSys obj1 = new BankSys();
		BankDeposit obj2 = new BankDeposit();
		BankWithdrawal obj3 = new BankWithdrawal();
		String opt = "";
		int a = 0;

		obj1.receipt();
		do {
			obj2.numDeposit();
			obj3.numWithdraw();
			obj2.amtDeposit();
			obj3.amtWithdraw();
			obj1.receipt2();
			System.out.println("Do you want any other transaction? if yes enter 'y' or enter any other key to Exit");
			Scanner sc = new Scanner(System.in);
			opt = sc.next();

			if (opt.equals("y")) {
				a = 1;
			} else {
				System.out.println("Thank you for using our Banking\n" + "Have a Nice Day");
			}
		} while (opt.equals("y") && a == 1);
	}

}
